function CLRU_Initialize()
% 该函数完成对CLRU的初始化，其中CLRU在主函数中也是一全局变量
global CLRU;
global buf_size;
% list存放的是访问的LPN号，这里的LPN号都是在原来的基础上加1偏移
CLRU.list=zeros(1,buf_size);
% 这里的size大小是指list中包含非0的项
CLRU.size=0;
end
function DelLPNInCLRU()
% 该函数删除CLRU中的LRU位置的数据
 global CLRU;
 global BlkTableArr;
% 需要用到的全局变量
 global PAGE_NUM_PER_BLK
 if CLRU.size<=0
        return;
 end
 Index=FindMinAge(CLRU.list);
%  Victim是删除的LPN号
 Victim=CLRU.list(Index);
 ResetNandPage(Victim);
%  注意这里的Victim不是Index
 CLRU.list(Index)=0;
 CLRU.size=CLRU.size-1;
% -----------------聚簇算法代码嵌入------------------------
%       计算更新对应的块索引标记
        tempBlkNum=fix(Victim/PAGE_NUM_PER_BLK)+1;
%       在块对应的Clist删除相应的CLRU位置索引Index
        BlkTableArr(tempBlkNum).Clist=DelVauleInArr(BlkTableArr(tempBlkNum).Clist,Index);   
%       对应的统计量相应的变化
        BlkTableArr(tempBlkNum).CleanNum=BlkTableArr(tempBlkNum).CleanNum-1;
        BlkTableArr(tempBlkNum).TotalSize=BlkTableArr(tempBlkNum).TotalSize-1;
%%-----------------------------做一个错误检测----------------------------------------------
        Clist_length=length(BlkTableArr(tempBlkNum).Clist);
        Dlist_length=length(BlkTableArr(tempBlkNum).Dlist);
        if(BlkTableArr(tempBlkNum).TotalSize~=Clist_length+Dlist_length)
            fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum, Dlist_length);
            fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
            fprintf('BlkTable(%d) -TotalSize is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).TotalSize);
            error('the add Index into BlkTableArr exist error!');
        end
        if (BlkTableArr(tempBlkNum).CleanNum~=Clist_length||BlkTableArr(tempBlkNum).DirtyNum~=Dlist_length)
            fprintf('BlkTable(%d) -CleanNum is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).CleanNum);
            fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
            fprintf('BlkTable(%d) -DirtyNum is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).DirtyNum);
            fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum,Dlist_length)
            error('the add Index into BlkTableArr exist error!');
        end
%-------------------------------错误检测结束------------------------------------------------
end
function HitInDLRU(ReqLPN)
% 该函数处理命中DLRU队列的请求
% 和外界数据交换的全局变量
    global NandPageArr;
    global CacheMaxAgeIndex;
    global DLRU;
    global CLRU;
    global Stat;
    global CDHit;
    NandPageArr(ReqLPN.start).Cache_Age=NandPageArr(CacheMaxAgeIndex).Cache_Age+1;
    CacheMaxAgeIndex=ReqLPN.start;
    if ReqLPN.type==0
        Stat.write_hit_count=Stat.write_hit_count+1;
        CDHit.DWH=CDHit.DWH+1;
    else
        Stat.read_hit_count=Stat.read_hit_count+1;
        CDHit.DRH=CDHit.DRH+1;
    end
%  这里不需要插入和聚簇相关的代码段
%  ------------------------------DEBUG-------------------------------------------
%              加入代码调试判断CLRU和DLRU的实际大小时候符合预期
           if (sum(CLRU.list>0)~=CLRU.size||sum(DLRU.list>0)~=DLRU.size)
               fprintf('the CLRU.size is %d\n',CLRU.size);
               fprintf('the CLRU.list nozeros entry num is %d\n',sum(CLRU.list>0)); 
               fprintf('the DLRU.size is %d\n',DLRU.size);
               fprintf('the DLRU.list noezeors entry num is %d\n',sum(DLRU.list>0));
               error('find error in HitInDLRU');
           end
%   --------------------------DEBUG-----------------------------------------
end
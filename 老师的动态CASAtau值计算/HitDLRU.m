function HitDLRU(LPN_index,req_type)  
% 该函数完成对CLRU命中的操作，根据不同的读写命中请求进行不同的操作
% req_type==0表示写请求命中，req_type!=0读请求命中
global DLRU;
global Stat;
% 根据读写操作不同，做不同统计信息操作
if req_type~=0
    Stat.read_hit_count=Stat.read_hit_count+1;
else
    Stat.write_hit_count=Stat.write_hit_count+1;
end
DLRU.LPN_list=[DLRU.LPN_list(:,LPN_index),DLRU.LPN_list(:,1:LPN_index-1),DLRU.LPN_list(:,LPN_index+1:end)];
Stat.read_hit_count=Stat.read_hit_count+1;

end
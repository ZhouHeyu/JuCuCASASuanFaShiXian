function DelLPNInDLRU()
% 该函数删除CLRU中的LRU位置的数据
global DLRU;
% 直接删除CLRU尾部的数据
% 后续可以根据访问位循环遍历Count来寻找替换项
% while(1)
%     CLRU.LPN_list(2,end)=CLRU.LPN_list(2,end)-1;
%     if CLRU.LPN_list(2,end)==0
%         break;
%     end
%     CLRU.LPN_list=[CLRU.LPN_list(:,end),CLRU.LPN_list(:,1:end-1)];
% end
% 加入CLRU大小判断检测
if DLRU.LPN_list_size<=0
    return;
end
DLRU.LPN_list=[DLRU.LPN_list(:,1:end-1)];
DLRU.LPN_list_size=DLRU.LPN_list_size-1;





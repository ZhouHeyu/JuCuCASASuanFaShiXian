%LRU_release.m

clc
clear all;
trace_filename='.\test_trace\financial1';%配置输入的trace文件
output_filename='.\result\LRU\trace_financial1_LRU_result_buffsize_6000_var.mat';%设置输出的指定文件
[req_arrive,device_num,req_sec_start,req_sec_size,req_type]=textread(trace_filename,'%f%d%d%d%d');
%读取trace文件中的五个参数（请求到达时间，设备号，请求扇区号起始位置，请求扇区大小，请求操作类型）


%设置缓冲区的大小
buf_size=6000;
% 缓冲区存的数为-1表示该位置是空闲的，buf的第二行是存请求的读写位，存的是-1表示未使用
buf=-ones(2,buf_size);
%输出参量的初始化
miss_cnt=0;
evict_cnt=0;
hit_rate=0;
write_hit_count=0;%写请求命中次数
read_hit_count=0;%读请求命中次数
write_miss_count=0;
read_miss_count=0;
write_back_count=0;
all_page_req_num=0;%统计所有的页请求次数
physical_read_count=0;
physical_write_count=0;

%对请求扇区序列进行次数统计,有多少行就是有多少次请求
all_req_sec_num=length(req_sec_start);

%关于页对齐的参数
SECT_NUM_PER_PAGE=4; %每个页包含多少个扇区
req_LPN_start=0;%req_LPN_start=req_sec_start/SECT_NUM_PER_PAGE
req_page_size=0;%请求的页大小,req_page_size=(req_sec_start+req_sec_size-1)/SECT_NUM_PER_PAGE-req_sec_start/SECT_NUM_PER_PAGE+1


%开始处理请求，进行循环
for req_index=1:all_req_sec_num
    %request_index表示当前请求在总请求序列中的次序
    %对扇区请求页对齐
    req_LPN_start=fix(req_sec_start(req_index)/SECT_NUM_PER_PAGE);
    req_page_size=fix((req_sec_start(req_index)+req_sec_size(req_index)-1)/SECT_NUM_PER_PAGE)-fix(req_sec_start(req_index)/SECT_NUM_PER_PAGE)+1;
    req_LPN_type=req_type(req_index);%当前的读写请求类型
    all_page_req_num=all_page_req_num+req_page_size;%统计所有的页请求次数
    while(req_page_size>0)
        %遍历buf 查看是否存有该LPN号,返回flag=1则命中
        flag=0;
        for buf_index=1:buf_size
            %这里修改代码注意req_list的第一列存的是LPN号，第二列存的是读写请求
            if buf(1,buf_index)==req_LPN_start
                %命中缓冲区
                %这里添加新的东西，记录下命中的是什么请求
                if req_LPN_type==0
                    write_hit_count=write_hit_count+1;
                    buf(2,buf_index)=1;%对于读请求命中页变为脏页
                else
                    read_hit_count=read_hit_count+1;
                end
                %依据LRU策略，将命中的页存到队列的头部
                buf=[buf(:,buf_index),buf(:,1:buf_index-1),buf(:,buf_index+1:end)];
                flag=1;
                break;
            end
        end
        %若flag=0则表示未命中
        if flag==0
            %先完成buf的剔除工作
            if buf(1,end)~=-1
             %如果buf最后尾部存有不等于-1的数，LPN号都为正整数，则认为队列满,
                if buf(2,end)==1
                 %检测剔除的页是否为脏页，脏页要统计回写次数
                    write_back_count=write_back_count+1;
                    physical_write_count=physical_write_count+1;
                end
                %统计buf的剔除次数
                evict_cnt=evict_cnt+1;
            end
            %在处理数据的加载
            %这里添加新的东西，记录下未命中的是什么请求
            physical_read_count=physical_read_count+1;
            if req_LPN_type==0
                write_miss_count=write_miss_count+1;
                buf_temp=[req_LPN_start,1]';
            else
                read_miss_count=read_miss_count+1;
                buf_temp=[req_LPN_start,0]';
            end
            %根据LRU策略把刚加载的数据放到队列的头部
            buf=[buf_temp,buf(:,1:end-1)];
            miss_cnt=miss_cnt+1;
        end
     req_page_size=req_page_size-1;%请求页完成一次，大小减一
     req_LPN_start=req_LPN_start+1;
    end%while_end
end
%计算命中率
hit_rate=double((all_page_req_num-miss_cnt))/all_page_req_num;
%计算写命中率
write_hit_rate=double(write_hit_count)/(write_hit_count+write_miss_count);
%计算读命中率
read_hit_rate=double(read_hit_count)/(read_hit_count+read_miss_count);
%输出参数
fprintf('the all_page_req_num is %d\n',all_page_req_num);
fprintf('the hit rate is %10.8f\n',hit_rate);
fprintf('the evict number is %d\n',evict_cnt);
fprintf('the  all_write_count is %d\n', write_miss_count+write_hit_count);
fprintf('the  write_hit_count is %d\n', write_hit_count);
fprintf('the write_hit_rate is %10.8f\n',write_hit_rate);
fprintf('the  all_read_count is %d\n', read_miss_count+read_hit_count);
fprintf('the read_hit_count is %d\n',read_hit_count);
fprintf('the read_hit_rate is %10.8f\n',read_hit_rate);
fprintf('the physical_write_count is %d\n',physical_write_count);
fprintf('the physical_read_count is %d\n',physical_read_count);
%保存结果到指定文件
clear req_arrive device_num req_sec_start req_sec_size req_type ;
clear buf buf_temp buf_index;
save(output_filename);
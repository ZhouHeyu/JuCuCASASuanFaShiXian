function DisplayCurrStat(CurrStat)
global FlashParameter;
fprintf('Current Stat Information:Read_H=%d, Read_M=%d, Write_H=%d, Write_M=%d\n',CurrStat.ReadHitCount,CurrStat.ReadMissCount,CurrStat.WriteHitCount,CurrStat.WriteMissCount);
% 输出当前的Interval的一些相关统计信息
fprintf('The estimated average response time is %7.4f\n\n',CurrStat.AveDelay);
end
% 分析负载的访问频次的分布，读写操作的比例

clc
clear all;
trace_filename='Financial1';%配置输入要分析的trace文件
output_filename='Financial1_result.mat';%设置输出的指定文件
[req_arrive,device_num,req_sec_start,req_sec_size,req_type]=textread(trace_filename,'%f%d%d%d%d');
%读取trace文件中的五个参数（请求到达时间，设备号，请求扇区号起始位置，请求扇区大小，请求操作类型）

all_page_req_num=0;%统计所有的页请求次数

%对请求扇区序列进行次数统计,有多少行就是有多少次请求
all_req_sec_num=length(req_sec_start);

%关于页对齐的参数
SECT_NUM_PER_PAGE=4; %每个页包含多少个扇区
req_LPN_start=0;%req_LPN_start=req_sec_start/SECT_NUM_PER_PAGE
req_page_size=0;%请求的页大小,req_page_size=(req_sec_start+req_sec_size-1)/SECT_NUM_PER_PAGE-req_sec_start/SECT_NUM_PER_PAGE+1


% 插入统计需要的变量
max_req_sec_start=max(req_sec_start);
max_req_sec_size=max(req_sec_size);
M=fix((max_req_sec_start+max_req_sec_size)/SECT_NUM_PER_PAGE)+2;
% M=20000000;
LPN_F=zeros(1,M);
% 加入对操作间距的统计分析
% 需要加入系统操作时间S
S=0;
% 记录最后一次访问的时间戳
% 根据寻找最小数定义无穷大为9999999999
INF=9999999999;
LPN_lastvisit=zeros(1,M);
% 记录每个LPN最大访问间隔
LPN_MaxInterver=-ones(1,M);
% 记录每个LPN最小访问时间间隔
LPN_MinInterver=ones(1,M)*INF;
% 记录每个LPN在该负载条件下的平均访问的操作间隔
LPN_MeanInterver=zeros(1,M);
%统计所有操作间隔的平均
InterverAverage=0;
% financial1的最大扇区地址是1215633，则和2K对齐LPN范围在305000左右，下标表示为该LPN，存的是访问频次
% 读次数
write_count=0;
read_count=0;
average_sum=0;
% 为了测算精确的操作间距：两个相同操作间距间操作不同页的个数（相同页的重复操作算作一次操作）
% 引入存放当前历史记录队列ghost
ghost=[];
for req_index=1:all_req_sec_num
    %request_index表示当前请求在总请求序列中的次序
    %对扇区请求页对齐
    req_LPN_start=fix(req_sec_start(req_index)/SECT_NUM_PER_PAGE);
    req_page_size=fix((req_sec_start(req_index)+req_sec_size(req_index)-1)/SECT_NUM_PER_PAGE)-fix(req_sec_start(req_index)/SECT_NUM_PER_PAGE)+1;
    req_LPN_type=req_type(req_index);%当前的读写请求类型
    all_page_req_num=all_page_req_num+req_page_size;%统计所有的页请求次数
%    统计读写次数
    if req_LPN_type==0
        write_count=write_count+req_page_size;
    else
        read_count=read_count+req_page_size;
    end
    while(req_page_size>0)
% 7-22修改版本加入对操作间距的统计
        S=S+1;
%        对应的LPN访问频次加1,为防止索引出现为0，所偶的req_LPN_start在原基础上加1
         req_LPN_start=req_LPN_start+1;
%          bug调试，统计的访问频次时间
         LPN_F(req_LPN_start)=LPN_F(req_LPN_start)+1;
% 7-22修改版本加入对操作间距的统计,计算操作间距
% 更新最大操作间距和最小操作间距
%          temp=S-LPN_lastvisit(req_LPN_start);
%          temp不再是简单的当系统时间-最后访问的操作时间，而是查看当前ghost队列中不同操作间距
%          从ghost的尾部开始寻找LPN匹配项，先判断该ghost是否为空
           ghost_length=length(ghost);
           evict_flag=0;
           frist_visit=0;
           if ghost_length>0
%          遍历当前ghost队列查找匹配项，evict_flag=1表示找到匹配项
               for i=ghost_length:-1:1
                   if ghost(i)==req_LPN_start
                       evict_index=i;
                       evict_flag=1;
                       break;
                   end
               end
               if evict_flag==1
%              计算操作距离temp
                   temp=ghost_length-evict_index;
%               从ghost队列中剔除该项，之后尾部加入该项
                   ghost=[ghost(1:evict_index-1),ghost(evict_index+1:end)];
               else
%              没找到，说明是第一次访问，置为frist_visit=1,不予计算操作间距
                    frist_visit=1;
               end
           else
%               当前ghost队列为空，加入的访问当然是第一次访问
                frist_visit=1;
           end
%          最后把访问的LPN加入到ghost队列的尾部
            ghost=[ghost(1:end),req_LPN_start];
%         根据前面的frist_visit的值操作，来决定是否更新操作间距
         if frist_visit==0 
%         更新每一次平均操作的间距
             average_sum=average_sum+temp;
             if temp>LPN_MaxInterver(req_LPN_start)
                LPN_MaxInterver(req_LPN_start)=temp;
             end
             if temp<LPN_MinInterver(req_LPN_start)
                 LPN_MinInterver(req_LPN_start)=temp;
             end
%   更新每个请求的操作平均间距
             if LPN_MeanInterver(req_LPN_start)==0
                 LPN_MeanInterver(req_LPN_start)=temp;
             else
                 MeanTemp=(LPN_MeanInterver(req_LPN_start)*(LPN_F(req_LPN_start)-1)+temp)/LPN_F(req_LPN_start);
%         这里做一个粗略的化整
                 LPN_MeanInterver(req_LPN_start)=round(MeanTemp);
             end
         end
% 最后更新下最后访问的操作时间
         LPN_lastvisit(req_LPN_start)=S;
         req_page_size=req_page_size-1;%请求页完成一次，大小减一
    end
end
InterverAverage=round(average_sum/S);
% 将统计后的操作间隔，按降序排序
% [MaxInter,LPNMaxInterIndex]=sort(LPN_MaxInterver,'descend');
% [MinInter,LPNMinInterIndex]=sort(LPN_MinInterver,'descend');
% [MeanInter,LPNMeanInterIndex]=sort(LPN_MeanInterver,'descend');
% figure(1);
% subplot(3,1,1);
% plot(MaxInter(1:500));
% title('最大操作间隔');
% subplot(3,1,2);
% plot(MinInter(1:10000));
% title('最小操作间隔');
% subplot(3,1,3);
% plot(MeanInter(500:1000));
% title('平均操作间隔前500项');

fprintf('该负载的平均操作间距为%d\n',InterverAverage);
% 将统计后的频次，按降序排序
[F,LPNF_DescendIndex]=sort(LPN_F,'descend');
% 查看负载访问频次前N项项的操作间隔
N=10000;
index=LPNF_DescendIndex(1:N);
ShowLPNMeanInterver=zeros(1,N);
ShowLPNMaxInterver=zeros(1,N);
ShowLPNMinInterver=zeros(1,N);
for i=1:N
    ShowLPNMeanInterver(i)=LPN_MeanInterver(index(i));
    ShowLPNMaxInterver(i)=LPN_MaxInterver(index(i));
    ShowLPNMinInterver(i)=LPN_MinInterver(index(i));
end
figure(1);
subplot(3,1,1);
plot(ShowLPNMaxInterver);
title('最大操作间隔');
subplot(3,1,2);
plot(ShowLPNMinInterver);
title('最小操作间隔');
subplot(3,1,3);
plot(ShowLPNMeanInterver);
title('平均操作间隔');

figure(2)
fprintf('the all_page_req_num is %d\n',all_page_req_num);
fprintf('the write_count is %d\n',write_count);
fprintf('the read_count is %d\n',read_count);
pie([read_count,write_count]);
clear req_arrive device_num req_sec_start req_sec_size req_type
save(output_filename);
% 该文件是对BlcokHeatStaticAnalysis输出得到的数据进行进一步的数据处理
% 上述函数得到的处理结果包含两个结构体，辅助结构体Trace
% PageStat,BlockStat主要针对AvePIRD和Frequency进行处理
clc
clear all
% 加载结果文件
load('Financial2_result.mat');
% 得到的PageStat.AvePIRD需要处理，一部分的页可能只访问了一次或者一次都没有访问，所以AvePIRD为0
% 计算得到所有的为零的项个数
ZeroEntryNum=sum((PageStat.AvePIRD==0));
NoZeroEntryNum=length(PageStat.AvePIRD)-ZeroEntryNum;
% 找到PageStat.AvePIRD最大的项，将所有PageStat.AvePIRD按照升序排序
[AscendAvePIRD,AscendIndex]=sort(PageStat.AvePIRD,'ascend');
% 通过移位操作移除掉前面所有为0的统计项
AscendAvePIRD=AscendAvePIRD(ZeroEntryNum+1:end);
AscendIndex=AscendIndex(ZeroEntryNum+1:end);
% % 通过比例alpha调整多大的AvePIRD是大多数访问的上线
% alpha=0.47;
% PIRDThresholdIndex=fix(length(AscendIndex)*alpha);
% AvePIRDThreshold=AscendAvePIRD(PIRDThresholdIndex);
% 通过上述的初步调测47%访问页的AvePIRD低于250左右
PThreshold=1:10:10000;
alpha_voild=zeros(1,length(PThreshold));
alpha_all=zeros(1,length(PThreshold));
for index=1:length(PThreshold)
    Temp=AscendAvePIRD<PThreshold(index);
%   剔除那些没访问的页，占有效访问页的占比
    alpha_voild(index)=sum(Temp)/NoZeroEntryNum;
    alpha_all(index)=sum(Temp)/(NoZeroEntryNum+ZeroEntryNum);
end
subplot(2,1,1);
plot(PThreshold,alpha_voild);
xlabel('AvePThreshold');
ylabel('低于该阈值的访问页占总有效请求的比值');
subplot(2,1,2);
plot(PThreshold,alpha_all);
xlabel('AvePThreshold');
ylabel('低于该阈值的访问页占全部请求页的比值');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 对单个阈值做下分析块分布比例
% 通过上述的负载（45%/25%）的AvePIRD低于100-200左右，估计150(Hot_PThreshold),
% Hot_PThreshold=150;
% % 根据上面的AscendIndex就是对应页的LPN号，找到这些LPN分布在哪些块中
% L=sum((AscendAvePIRD<Hot_PThreshold));
% % 采用简单的桶排序计数
% blk_entry=zeros(1,Trace.UpBlockBound);
% % 设置每个块的包含多少的页
% PER_BLK_NUM_PAGE=64;
% % 统计多少块包含了这些所谓的热数据页
% for index=1:L
% %     将对应的LPN转化成LBN，累加其Blk_entry上的值
%     LBN=ceil(AscendIndex(index)/PER_BLK_NUM_PAGE);
%     blk_entry(LBN)=blk_entry(LBN)+1;
% end
% % 计算其占总块数的占比
% HotPage_Inblk_ratio=sum((blk_entry>0))/Trace.UpBlockBound;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 干脆做个对比实验不同的PThreshold的块分布比例做分析
% % % PThreshold和上述的一样
HotPage_Inblk_ratio=zeros(1,length(PThreshold));
for Pindex=1:length(PThreshold)
%     根据上面的AscendIndex就是对应页的LPN号，找到这些LPN分布在哪些块中
%   统计大概有多少低于该阈值的数据页L个数
    L=sum((AscendAvePIRD<PThreshold(Pindex)));
%     采用简单的桶排序计数
    blk_entry=zeros(1,Trace.UpBlockBound);
%     设置每个块的包含多少的页
    PER_BLK_NUM_PAGE=64;
%     统计多少块包含了这些所谓的热数据页
    for index=1:L
%         将对应的LPN转化成LBN，累加其Blk_entry上的值
        LBN=ceil(AscendIndex(index)/PER_BLK_NUM_PAGE);
        blk_entry(LBN)=blk_entry(LBN)+1;
    end
%     计算其占总块数的占比
    HotPage_Inblk_ratio(Pindex)=sum((blk_entry>0))/Trace.UpBlockBound;
end
figure(2)
plot(PThreshold,HotPage_Inblk_ratio);
xlabel('AvePThreshold');
ylabel('包含低于这些阈值（热数据）的块占总的块的比例');


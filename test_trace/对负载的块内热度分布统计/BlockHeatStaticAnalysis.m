% 以下代码是对输入的负载每个块的热度分布进行统计分析
% 实验的目标是为了验证块（64个页的块）的是否大部分为冷页，少数页为热页，这个比例大概是多少？
% 为了验证这个思路，需要确定什么样的页是热的数据页
% 一种就是在总体负载中访问次数最高的页定义为热的数据页，一种是访问过程中访问间距较短的页（针对有限的缓冲区而言）这些页是热的需要在一定时间需要考虑保留的
% 因此我们不妨统计这两种情况的数据页在各自块中的分布比例，和包含这些数据页的块占总的块的比例
clc
clear all;
trace_filename='../Financial2';%配置输入要分析的trace文件
output_filename='Financial2_result.mat';%设置输出的指定文件
global ReqSec;
[ReqSec.arrive,ReqSec.device_num,ReqSec.start,ReqSec.size,ReqSec.type]=textread(trace_filename,'%f%d%d%d%d');
% %读取trace文件中的五个参数（请求到达时间，设备号，请求扇区号起始位置，请求扇区大小，请求操作类型）
[MaxReqSec,MaxReqSecIndex]=max(ReqSec.start);
% SizeOfMaxReqSec=ReqSec.size(MaxReqSecIndex);
SizeOfMaxReqSec=max(ReqSec.size);
%%
% 这里一个页包含4个扇区，那么一个
PER_PAGE_NUM_SEC=4;
PER_BLOCK_NUM_PAGE=64;
PER_BLOCK_NUM_SEC=PER_BLOCK_NUM_PAGE*PER_PAGE_NUM_SEC;
% 定义Trace结构体，统计仿真负载的一些特性
% 计算出最大的扇区边界
Trace.UpSecBound=MaxReqSec+SizeOfMaxReqSec;
% 向上取整，确保包含全部的扇区
Trace.UpPageBound=ceil(Trace.UpSecBound/PER_PAGE_NUM_SEC);
% 向上取整，确保包含全部的页
Trace.UpBlockBound=ceil(Trace.UpPageBound/PER_BLOCK_NUM_PAGE);
% 计算负载的数据库地址容量是XGB
Trace.Vessel=Trace.UpBlockBound*PER_BLOCK_NUM_SEC*512/(1024*1024*1024);
% 负载全部的访问请求个数
Trace.all_req_num=length(ReqSec.start);
% 负载全部的访问请求页数
Trace.all_page_req_num=0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%根据上述的上界预设统计的所需要的数组大小
%%
%页的统计信息
%AvePIRD表示单个页的平均访问间距
PageStat.AvePIRD=zeros(1,Trace.UpPageBound);
%MaxPIRD表示单个页最大的访问间距（以操作页为单位）
PageStat.MaxPIRD=zeros(1,Trace.UpPageBound);
%MinPIRD表示单个页最小的访问间距
PageStat.MinPIRD=zeros(1,Trace.UpPageBound);
%Frequency表示单个页在负载访问过程中被访问的全部频次
PageStat.Frequency=zeros(1,Trace.UpPageBound);
%%
% 块的统计信息
% AveBIRD表示单个块的平均访问间距（以操作页为单位）
BlockStat.AveBIRD=zeros(1,Trace.UpBlockBound);
% MaxBIRD表示单个块的最大访问间距
BlockStat.MaxBIRD=zeros(1,Trace.UpBlockBound);
% MinBIRD表示单个块的最小访问间距(以操作页为单位)；
BlockStat.MinBIRD=zeros(1,Trace.UpBlockBound);
% Frequency表示单个块的总的访问频次
BlockStat.Frequency=zeros(1,Trace.UpBlockBound);
%%
% 有关下面循环遍历需要用到的变量
% SysClock表示当前处理的系统虚拟时间（以操作页个数为单位）
SysClock=1;
% PageLastAccsess表示对应页最后一次访问的系统时间
PageLastAccess=zeros(1,Trace.UpPageBound);
BlockLastAccess=zeros(1,Trace.UpBlockBound);
% 导入负载开始遍历统计信息
for req_index=1:Trace.all_req_num
    [ReqLPN]=PageAlignment(req_index);
    Trace.all_page_req_num=Trace.all_page_req_num+ReqLPN.size;%统计所有的页请求次数
      while(ReqLPN.size>0)
%       将当前请求的LPN页号转化成对应的块号，考虑到页号从0开始，因此对所有的页都加1
        CurrLPN=ReqLPN.start+1;
        CurrLBN=ceil(CurrLPN/PER_BLOCK_NUM_PAGE);
% %       考虑CurrLPN=0计算得到CurrLBN=0的特殊情况
%         if CurrLBN==0
%             CurrLBN=1;
%         end
%       如果对应的页请求访问命中，对应下标的PageStat.Frequcency+1，
        PageStat.Frequency(CurrLPN)=PageStat.Frequency(CurrLPN)+1;
        BlockStat.Frequency(CurrLBN)=BlockStat.Frequency(CurrLBN)+1;
%%
%       计算这次访问和上次访问的操作间距（PIRD和BIRD），针对第一访问的需要做一个处理
        if PageLastAccess(CurrLPN)==0
%            认为第一次访问的操作间距为-1
            TempPIRD=-1;
        else
            TempPIRD=SysClock-PageLastAccess(CurrLPN);

        end
%       处理BIRD
        if BlockLastAccess(CurrLBN)==0
%            认为第一次访问的操作间距为-1
            TempBIRD=-1;
        else
            TempBIRD=SysClock-BlockLastAccess(CurrLBN);
        end
%       处理完BIRD
%       更新PagePIRD和BlockBIRD的最大和最小的
        if TempPIRD>0
%           不记录第一访问的造成无效的操作间隔，在后续统计中发现MaxPIRD和MinPIRD=0结合Frequency判断是否为第一次访问
            if TempPIRD>PageStat.MaxPIRD(CurrLPN)
                PageStat.MaxPIRD(CurrLPN)=TempPIRD;
            end
            if (TempPIRD<=PageStat.MinPIRD(CurrLPN)||PageStat.MinPIRD(CurrLPN)==0)
    %             考虑到初始化的时候最小初始的都是0，所以针对第一次的全都记作为最小
                    PageStat.MinPIRD(CurrLPN)=TempPIRD;
            end
        end
%       针对PageStat(Min-Max)更新处理完成
        if TempBIRD>0
%           不记录第一次访问造成的无效间距
            if TempBIRD>BlockStat.MaxBIRD(CurrLBN)
                BlockStat.MaxBIRD(CurrLBN)=TempBIRD;
            end
            if (TempBIRD<=BlockStat.MinBIRD(CurrLBN)||BlockStat.MinBIRD(CurrLBN)==0)
    %             考虑到初始化的时候最小初始的都是0，所以针对第一次的全都记作为最小
                    BlockStat.MinBIRD(CurrLBN)=TempBIRD;
            end
        end
%       针对BlockStat(Min-Max)更新处理完成
%       结合访问频次和上次记录平均PIRD和BIRD，更新当前的BIRD（对BIRD有问题，一个连续请求内反复访问一个块。。。）
        if TempPIRD>0
%             第一次访问的其操作间距统一为-1是无效的都忽略
%            第2次访问，总操作间隔次数为1，上次初始值为0，前一串计算值为0，符合计算逻辑
%           因为上面先更新了命中频次，所以要减1表示上次统计的频次，再减1表示上次统计总命中存在几次间隔
            SumPIRD=(PageStat.Frequency(CurrLPN)-2)*PageStat.AvePIRD(CurrLPN)+TempPIRD;
            PageStat.AvePIRD(CurrLPN)=SumPIRD/(PageStat.Frequency(CurrLPN)-1);
        end
        
        if TempBIRD>0
            SumBIRD=(BlockStat.Frequency(CurrLBN)-2)*BlockStat.AveBIRD(CurrLBN)+TempBIRD;
            BlockStat.AveBIRD(CurrLBN)=SumBIRD/(BlockStat.Frequency(CurrLBN)-1);
        end
%       统计更新完AveBIRD和AvePIRD
%%        
%       更新当前对应块和页最后一次访问时间为当前的系统时间
        PageLastAccess(CurrLPN)=SysClock;
        BlockLastAccess(CurrLBN)=SysClock;
%       一次页请求统计完成
        ReqLPN.size=ReqLPN.size-1;%请求页完成一次，大小减一
        ReqLPN.start=ReqLPN.start+1;
        SysClock=SysClock+1;
      end%end-of-while
end%end-of-for



%%
% 保持结果,清除掉一些不用的中间变量
clear ReqSec
save(output_filename);












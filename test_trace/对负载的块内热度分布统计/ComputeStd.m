clc
clear all
% 加载结果文件
load('Financial1_result.mat');
% 计算每个块内页的AvePIRD的方差情况
% 每个块包含的页数是64
PER_BLK_NUM_PAGE=64;
BlockSTD=zeros(1,Trace.UpBlockBound);
TempBlockArr=zeros(1,PER_BLK_NUM_PAGE);
for index=1:Trace.UpBlockBound
%    因为在之前的代码中，确定页上限后，在确定块上限，没有反过来计算更新最大地址页上限的时候出问题了
%   这里做了代码的小调整
    Ed=index*PER_BLK_NUM_PAGE;
    if Ed>Trace.UpPageBound
        Ed=Trace.UpPageBound;
    end
    TempBlockArr=PageStat.AvePIRD((index-1)*PER_BLK_NUM_PAGE+1:Ed);
    BlockSTD(index)=std(TempBlockArr,0,2);
end
% 计算得到每组块的中对AvePIRD的方差分布情况
% 初步的方差计算（统计中包含了没有访问的数据块和数据页）
figure(1)
plot(BlockSTD);
% 进一步准确地计算（计算同一块的方差的时候抛弃没有访问的页）
for index=1:Trace.UpBlockBound
     Ed=index*PER_BLK_NUM_PAGE;
    if Ed>Trace.UpPageBound
        Ed=Trace.UpPageBound;
    end
    TempBlockArr=PageStat.AvePIRD((index-1)*PER_BLK_NUM_PAGE+1:Ed);
%   删除其中AvePIRD为0的项，减小对方差的准确计算的影响
    SecondTemp=[];
    L=length(TempBlockArr);
    for Sindex=1:L
        if TempBlockArr(Sindex)~=0
            SecondTemp=[SecondTemp,TempBlockArr(Sindex)];
        end
    end
%   计算那些非零的AvePIRD项的Std
    L=length(SecondTemp);
    if L==0
%     说明该块根本就没被访问过
        BlockSTD(index)=0;
    else
        BlockSTD(index)=std(SecondTemp,0,2);
    end

end
% 输出结果图
figure(2)
plot(BlockSTD);


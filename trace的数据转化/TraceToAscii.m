clc
clear all;
trace_filename='B1';%配置输入的trace文件
result_filename='B1.txt';
[req_arrive,device_num,req_sec_start,req_sec_size,req_type]=textread(trace_filename,'%f%d%d%d%d');
% %读取trace文件中的五个参数（请求到达时间，设备号，请求扇区号起始位置，请求扇区大小，请求操作类型）

L=length(req_type);
t=zeros(L,1);
% 写入数据，数据处理过程，20000003表示local,time_critical 读请求-----|---20000002就表示相应的写请，奇偶判断
% 判断最高位2本地请求，4顺序请求
fp=fopen(result_filename,'a');
for index=1:L
    if mod(req_type(index),2)==0
%         req_type(index)=0;
            t(index)=0;
    else
%         req_type(index)=1;
            t(index)=1;
    end
    fprintf(fp,'%f %d %d %d %d\n',req_arrive(index),device_num(index),req_sec_start(index),req_sec_size(index),t(index));
end
fclose(fp);
% 清除中间变量
% 核实数据的读写比例是否满足预期的
read_ratio=sum(t)/L;
% 核实数据的本地local比例是否符合预期(根据req_flag进行统计)2000-000X表示本地请求
LOCAL=(fix(req_type/10000000))==2;
local_ratio=sum(LOCAL)/L;
% 核实数据的顺序性比例4000-000X
SEQ=(fix(req_type/10000000))==4;
seq_ratio=sum(SEQ)/L;
fprintf('the read ratio is %f\n',read_ratio);
fprintf('the local ratio is %f\n',local_ratio);
fprintf('the seq ratio is %f\n',seq_ratio);
fprintf('all requset number is %d\n',L);
% 还要统计最大地址范围
address_sec=max(req_sec_start+req_sec_size);
fprintf('the Max Sec_address is %d\t the database is %f GB\n',address_sec,address_sec*512/1024/1024/1024);
% 统计访问请求大大小
fprintf('the Max req_size is %d\t the Average req_size is %f\n',max(req_sec_size),mean(req_sec_size));
% 统计最大时间
fprintf('the frist access time is %f\t the last access time is %f\n',min(req_arrive),max(req_arrive));
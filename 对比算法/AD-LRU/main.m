clc
clear all
trace_filename='..\..\test_trace\Syn1-5\syn1.txt';%配置输入的trace文件
output_filename='.\AD-LRU-result\Syn1_ADLRU_result_buff8M_alpha0.2_var.mat';%设置输出的指定文件
global ReqSec;
[ReqSec.arrive,ReqSec.device_num,ReqSec.start,ReqSec.size,ReqSec.type]=textread(trace_filename,'%f%d%d%d%d');
%读取trace文件中的五个参数（请求到达时间，设备号，请求扇区号起始位置，请求扇区大小，请求操作类型(0表示写请求，1读请求)）

% 设置缓冲区的大小
% 关于读写代价的设置和写放大系数的调整
global FlashParameter;
FlashParameter.rCost=50;
FlashParameter.wCost=250;
FlashParameter.wAmp =1.35;

% 关键的全局变量的定义
% global LPNInfo;
global Stat;
global HotLRU;
global ColdLRU;
% 关键的全局变量定义
% 冷队列占缓冲区最小的比例值
alpha=0.2;
% 设计缓冲区的大小
buf_size=1024*4;
Min_limit=fix(buf_size*alpha);
% 初始化HotLRU,CleanLRU,Stat这些结构体
HotLRU_Initalize();
ColdLRU_Initalize();
Stat_Initalize();
%StatLast作为上次统计的情况
StatLast=Stat;
% 设定观察周期
Interval=10000;
Interval_index=0;
%对请求扇区序列进行次数统计,有多少行就是有多少次请求
all_req_sec_num=length(ReqSec.start);
%增加存储每个Interval期间内的Write/ReadHitCount/MissCount的情况
global history;
InitHistoryStat(round(all_req_sec_num/Interval));
for req_index=1:all_req_sec_num
%     统计和输出观察周期内的统计数据
     if mod(req_index,Interval)==0
        Interval_index=Interval_index+1;
        fprintf('req=%d, HotLRU_length=%d, ColdLRU_length=%d \n',req_index,HotLRU.length,ColdLRU.length);        
        CurrStat.ReadHitCount=Stat.read_hit_count-StatLast.read_hit_count;
        CurrStat.ReadMissCount=Stat.read_miss_count-StatLast.read_miss_count;
        CurrStat.WriteHitCount=Stat.write_hit_count-StatLast.write_hit_count;
        CurrStat.WriteMissCount=Stat.write_miss_count-StatLast.write_miss_count;
        %Interval期间内发生了多少次的页请求
        CurrStat.TotalCount    =Stat.all_page_req_num-StatLast.all_page_req_num;
        CurrStat.AveDelay=(double(CurrStat.ReadMissCount*FlashParameter.rCost+CurrStat.WriteMissCount*FlashParameter.wCost*FlashParameter.wAmp)/CurrStat.TotalCount);
        DisplayCurrStat(CurrStat);
%         UpdateHistoryStat(Interval_index,CurrStat,tau,CLRU.LPN_list_size,DLRU.LPN_list_size);
        UpdateHistoryStat(Interval_index,CurrStat,HotLRU.length,ColdLRU.length);
        StatLast=Stat;
     end
    %request_index表示当前请求在总请求序列中的次序
    %对扇区请求页对齐
    [ReqLPN]=PageAlignment(req_index);
    Stat.all_page_req_num=Stat.all_page_req_num+ReqLPN.size;%统计所有的页请求次数
    CurSize = ReqSec.size(req_index);
    while(ReqLPN.size>0)
        % 针对一个LPN，先遍历缓冲区，查看是否存在LPN
        %输入AD-LRU算法的核心逻辑代码
        InCold=0;
        InHot=0;
%       判断是否存在ColdLRU,
        [InCold]=FindLPNinLRU(ReqLPN.start,ColdLRU);
        if InCold~=0
%          如果命中Cold，则将二次命中的数据移动到HotLRU
            if ReqLPN.type==0
                Stat.write_hit_count=Stat.write_hit_count+1;
            else
                Stat.read_hit_count=Stat.read_hit_count+1;
            end
            [ColdLRU,HotLRU]=ColdToHot(InCold,ReqLPN.type,ColdLRU,HotLRU);
        else
            [InHot]=FindLPNinLRU(ReqLPN.start,HotLRU);
        end
%       如果命中HotLRU
        if InHot~=0
            if ReqLPN.type==0
                Stat.write_hit_count=Stat.write_hit_count+1;
%               访问位重置１
                HotLRU.list(3,InHot)=1;
%                脏页标识置位
                HotLRU.list(2,InHot)=1;
            else
                Stat.read_hit_count=Stat.read_hit_count+1;
%                访问位重置１
                HotLRU.list(3,InHot)=1;               
            end
%           将命中的数据项移动到队列的MRU位置
            HotLRU.list=[HotLRU.list(:,InHot),HotLRU.list(:,1:InHot-1),HotLRU.list(:,InHot+1:end)];
%           注意更新对应的FC指针
            [HotLRU]=UpdateLRUFC(HotLRU);
        end
%       如果没用命中缓冲区
        if (InHot+InCold)==0
%           首先判断缓冲区是否满
            Stat.miss_count=Stat.miss_count+1;
            if (HotLRU.length+ColdLRU.length)>=buf_size
                if ColdLRU.length>Min_limit
%                  选择对应的Cold删除
                    [ColdLRU]=DelLPNInLRU(ColdLRU);
                else
%                  选择对应的HotLRU删除
                    [HotLRU]=DelLPNInLRU(HotLRU);
                end
            end
%               缓冲区满删除操作完成
%               加载新的数据进入buffer-进入ColdLRU(都要加载物理读操作)
                Stat.physical_read_count=Stat.physical_read_count+1;
                if ReqLPN.type==0
                    Stat.write_miss_count=Stat.write_miss_count+1;
                    temp=[ReqLPN.start,1,1]';
                else
                    Stat.read_miss_count=Stat.read_miss_count+1;
                    temp=[ReqLPN.start,0,1]';
                end
                ColdLRU.list=[temp,ColdLRU.list(:,1:end)];
                ColdLRU.length=ColdLRU.length+1;
                [ColdLRU]=UpdateLRUFC(ColdLRU);
        end%缓冲区未命中操作完成
        ReqLPN.size=ReqLPN.size-1;%请求页完成一次，大小减一
        ReqLPN.start=ReqLPN.start+1;
    end%end-of-while
    
end%end-for

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% 数据处理部分
%计算命中率
hit_rate=double((Stat.all_page_req_num-Stat.miss_count))/Stat.all_page_req_num;
%计算写命中率
write_hit_rate=double(Stat.write_hit_count)/(Stat.write_hit_count+Stat.write_miss_count);
%计算读命中率
read_hit_rate=double(Stat.read_hit_count)/(Stat.read_hit_count+Stat.read_miss_count);

%输出参数
fprintf('the all_page_req_num is %d\n',Stat.all_page_req_num);
fprintf('the hit rate is %4.2f\n',hit_rate);
fprintf('the  all_write_count is %d\n', Stat.write_miss_count+Stat.write_hit_count);
fprintf('the  write_hit_count is %d\n', Stat.write_hit_count);
fprintf('the write_hit_rate is %4.2f\n',write_hit_rate);
fprintf('the  all_read_count is %d\n', Stat.read_miss_count+Stat.read_hit_count);
fprintf('the read_hit_count is %d\n',Stat.read_hit_count);
fprintf('the read_hit_rate is %4.2f\n',read_hit_rate);

fprintf('the estimated average response time is %7.4f\n',(double(Stat.read_miss_count*FlashParameter.rCost+Stat.write_miss_count*FlashParameter.wCost*FlashParameter.wAmp)/Stat.all_page_req_num));

% 剔除操作和回写操作次数
% fprintf('the evict number is %d\n',evict_count);
fprintf('the write_back_count is %d\n',Stat.write_back_count);
fprintf('the Stat.physical_read_count is %d\n',Stat.physical_read_count);
fprintf('the Stat.physical_write_count is %d\n',Stat.physical_write_count);
%保存结果到指定文件
% 剔除一些不必要的中间数据，节省保存
clear ReqSec;
save(output_filename);
function UpdateHistoryStat(index,CurrStat,HotLRU_length,ColdLRU_length)
global history;
history.ReadHitCount(index)=CurrStat.ReadHitCount;
history.ReadMissCount(index)=CurrStat.ReadMissCount;
history.WriteHitCount(index)=CurrStat.WriteHitCount;
history.WriteMissCount(index)=CurrStat.WriteMissCount;
history.TotalCount(index)=CurrStat.TotalCount;
history.AveDelay(index)=CurrStat.AveDelay;
% 添加其他相关算法的代码
history.HotLRU_length(index)=HotLRU_length;
history.ColdLRU_length=ColdLRU_length;
% % history.tau(index)=tau;CASA算法
% history.CLRU_length(index)=CL;
% history.DLRU_length(index)=DL;
end
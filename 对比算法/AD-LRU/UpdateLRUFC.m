function [LRU]=UpdateLRUFC(LRU)
% Fc是指向该队列中最近最少访问的干净页的指针,如果不存在，则返回0
    LRU.FC=0;
%   debug test
     [L,C]=size(LRU.list);
    if LRU.length~=C
%         代码复用，错误判断可能出现两种更新错误，在主函数需要判断
           error('LRU_length Update May Exist Error!\n');
    end
%    handle for LRU is empty
    if LRU.length==0
        return ;
    end
%    search the LRU-clean page in LRU.list
    for index=LRU.length:-1:1
        if LRU.list(2,index)==0
            LRU.FC=index;
            break;
        end
    end
    return ;
end
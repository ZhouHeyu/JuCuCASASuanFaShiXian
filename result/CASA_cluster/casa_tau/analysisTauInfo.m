clc
clear all
%利用仿真给出的history分析下每个interval中tau和各个变量间的关系
openfile='trace_all_Financial1_cluster8_CASA_RW19_buf4096_var.mat';
%只分析history的结果
load(openfile,'history');

%观测tau和当前的CLRU和DLRU长度的变化关系
figure(1);
% plot(history.CLRU_length,'r');
% hold on
% plot(history.DLRU_length,'b-');
% hold on 
% plot(history.tau,'g');
% legend('CLRU-length','DLRU-length','casa-tau');
% 方便对比将所有的队列长度归一化到占bufsize的比列
bufsize=4086;
CLRU_length_ratio=history.CLRU_length./bufsize;
DLRU_length_ratio=history.DLRU_length./bufsize;
HalfBufSize=bufsize/2;
tau_ratio=history.tau/bufsize;
plot(CLRU_length_ratio,'r');
hold on
plot(DLRU_length_ratio,'b-');
hold on 
plot(tau_ratio,'g');
legend('CCLRU_length_ratio','DLRU_length_ratio','casa-tau-ratio');
title('CASA-tau：都归一化为bufsize大小的比例');

figure(2)
%对比tau和当前将期间内的读写miss的比例关系
ReadMissRatio=history.ReadMissCount./(history.ReadMissCount+history.ReadHitCount);
WriteMissRatio=history.WriteMissCount./(history.WriteMissCount+history.WriteHitCount);
subplot(2,1,1);
plot(ReadMissRatio,'r');
hold on
plot(WriteMissRatio,'b');
hold on
plot(tau_ratio,'g');
legend('ReadMissRatio','WriteMissRatio','casa-tau-ratio');
title('读Miss和写Miss占间隔期间内各自请求的比例：tau都归一化为bufsize大小的比例');
%读写miss的次数占总请求的比例
ReadMissOnTotal=history.ReadMissCount./history.TotalCount;
WriteMissOnTotal=history.WriteMissCount./history.TotalCount;
subplot(2,1,2);
plot(ReadMissOnTotal,'r');
hold on
plot(WriteMissOnTotal,'b');
hold on
plot(tau_ratio,'g');
legend('ReadMissOnTotal','WriteMissOnTotal','casa-tau-ratio');
title('读Miss和写Miss占间隔期间内总页请求的比例：tau都归一化为bufsize大小的比例');

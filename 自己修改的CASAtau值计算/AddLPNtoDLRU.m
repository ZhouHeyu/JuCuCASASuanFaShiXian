function AddLPNtoDLRU(LPN)
% 该函数完成把新的读请求的LPN加载到CLRU
global DLRU;
% 初始加载的LPN，第二行（访问频次）为1
buf_temp=[LPN,1]';
DLRU.LPN_list=[buf_temp,DLRU.LPN_list(:,:)];
DLRU.LPN_list_size=DLRU.LPN_list_size+1;
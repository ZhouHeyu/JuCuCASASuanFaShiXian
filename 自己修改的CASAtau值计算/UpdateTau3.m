function [tau]=UpdateTau3(old)
%关于读写代价Cr，Cw,这里考虑读写延迟比例
global DLRU;
global CLRU;
global buf_size;
global FlashParameter;
global CDHit;
CLRU_length=CLRU.LPN_list_size;
DLRU_length=DLRU.LPN_list_size;
Cr_tmp=FlashParameter.rCost;
Cw_tmp=FlashParameter.wCost*FlashParameter.wAmp;

B_CLRU = (CDHit.CRH*Cr_tmp+CDHit.CWH*Cw_tmp)/old;
B_DLRU = (CDHit.DRH*Cr_tmp+CDHit.DWH*Cw_tmp)/(buf_size-old);

tau = min(max(round(B_CLRU/(B_CLRU+B_DLRU)*buf_size),0.1*buf_size),0.9*buf_size);
% fprintf('B_CLRU=%8.4f,B_DLRU=%8.4f,tau=%d\n',B_CLRU,B_DLRU,tau);
%根据比例归一化，Cr+Cw=1
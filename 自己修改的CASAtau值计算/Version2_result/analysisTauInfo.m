clc
clear all
%利用仿真给出的history分析下每个interval中tau和各个变量间的关系
openfile='trace_all_Financial2_cluster1_CASA_RW19_buf4096_var.mat';
%只分析history的结果
load(openfile,'history')

%观测tau和当前的CLRU和DLRU长度的变化关系
figure(1);
bufsize=4086;
CLRU_length_ratio=history.CLRU_length./bufsize;
DLRU_length_ratio=history.DLRU_length./bufsize;
HalfBufSize=bufsize/2;
tau_ratio=history.tau/bufsize;
subplot(2,1,1);
plot(CLRU_length_ratio,'r');
hold on
plot(DLRU_length_ratio,'b-');
hold on 
plot(tau_ratio,'g');
legend('CCLRU_length_ratio','DLRU_length_ratio','casa-tau-ratio');
ylabel('度量归一化为bufsize大小的比例');
xlabel('输出观测周期是10000个请求为一个点')
title('干净页队列，脏页队列长度与Tau值的变化曲线')

% 增加分析统计周期内的读写负载比例
all_read_count=history.ReadHitCount+history.ReadMissCount;
all_write_count=history.WriteHitCount+history.WriteMissCount;
% AllReadRatio=all_read_count./history.TotalCount;
% AllWriteRatio=all_write_count./history.TotalCount;
AllReadRatio=all_read_count./(all_read_count+all_write_count);
AllWriteRatio=all_write_count./(all_read_count+all_write_count);
subplot(2,1,2)
plot(AllReadRatio,'r');
hold on
plot(AllWriteRatio,'b');
hold on
plot(tau_ratio,'g');
legend('AllReadRatio','AllWriteRatio','casa-tau-ratio');
ylabel('都归一化为bufsize大小的比例');
xlabel('输出观测周期是10000个请求为一个点')
title('读写负载变化特性与Tau值的变化曲线')

figure(2)
%对比tau和当前将期间内的读写miss的比例关系
ReadMissRatio=history.ReadMissCount./(history.ReadMissCount+history.ReadHitCount);
WriteMissRatio=history.WriteMissCount./(history.WriteMissCount+history.WriteHitCount);
subplot(2,1,1);
plot(ReadMissRatio,'r');
hold on
plot(WriteMissRatio,'b');
hold on
plot(tau_ratio,'g');
legend('ReadMissRatio','WriteMissRatio','casa-tau-ratio');
title('读Miss和写Miss占间隔期间内各自请求的比例：tau都归一化为bufsize大小的比例');
%读写miss的次数占总请求的比例
ReadMissOnTotal=history.ReadMissCount./history.TotalCount;
WriteMissOnTotal=history.WriteMissCount./history.TotalCount;
subplot(2,1,2);
plot(ReadMissOnTotal,'r');
hold on
plot(WriteMissOnTotal,'b');
hold on
plot(tau_ratio,'g');
legend('ReadMissOnTotal','WriteMissOnTotal','casa-tau-ratio');
title('读Miss和写Miss占间隔期间内总页请求的比例：tau都归一化为bufsize大小的比例');

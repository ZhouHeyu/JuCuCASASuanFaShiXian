function PrintResult(Stat)
% 关于读写代价的设置和写放大系数的调整
global FlashParameter;
% 数据处理部分
%计算命中率
hit_rate=double((Stat.all_page_req_num-Stat.miss_count))/Stat.all_page_req_num;
%计算写命中率
write_hit_rate=double(Stat.write_hit_count)/(Stat.write_hit_count+Stat.write_miss_count);
%计算读命中率
read_hit_rate=double(Stat.read_hit_count)/(Stat.read_hit_count+Stat.read_miss_count);

%输出参数
fprintf('the all_page_req_num is %d\n',Stat.all_page_req_num);
fprintf('the hit rate is %4.2f\n',hit_rate);
fprintf('the  all_write_count is %d\n', Stat.write_miss_count+Stat.write_hit_count);
fprintf('the  write_hit_count is %d\n', Stat.write_hit_count);
fprintf('the write_hit_rate is %4.2f\n',write_hit_rate);
fprintf('the  all_read_count is %d\n', Stat.read_miss_count+Stat.read_hit_count);
fprintf('the read_hit_count is %d\n',Stat.read_hit_count);
fprintf('the read_hit_rate is %4.2f\n',read_hit_rate);

fprintf('the estimated average response time is %7.4f\n',(double(Stat.read_miss_count*FlashParameter.rCost+Stat.write_miss_count*FlashParameter.wCost*FlashParameter.wAmp)/Stat.all_page_req_num));

% 剔除操作和回写操作次数
% fprintf('the evict number is %d\n',evict_count);
fprintf('the write_back_count is %d\n',Stat.write_back_count);
fprintf('the Stat.physical_read_count is %d\n',Stat.physical_read_count);
fprintf('the Stat.physical_write_count is %d\n',Stat.physical_write_count);
end
function index=FindMinAge(Arr,type)
% 输入的参数Arr可能是CLRU.list或者DLRU.list,函数返回的是list中存放的LPN对应的age最小的项索引Index
% 即队列的LRU位置项
% 如果没有找到对应的Index，则返回0
    global NandPageArr;
    MinAge=9999999999;
    index=0;
%   L是整个数组的大小
    L=length(Arr);
%   NL是数组中非零项的个数也就是有效数据
    NL=sum(Arr>0);
%   对应的队列为空则直接返回
    if NL==0
        return;
    end
    for i=1:L
% %       当非零项遍历完,没有必要再遍历了
% %         if(NL<=0)
% %             break;
% %         end
%       确保数组中该位置存放的是有效的LPN
        if Arr(i)>0
    %       找到最小的age则替换
            if(NandPageArr(Arr(i)).Cache_Age<MinAge)
                MinAge=NandPageArr(Arr(i)).Cache_Age;
                index=i;
            end
%             NL=NL-1;
        end
    end%end-for
    return ;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%上述的代码运算太慢了，改用逻辑运算


end
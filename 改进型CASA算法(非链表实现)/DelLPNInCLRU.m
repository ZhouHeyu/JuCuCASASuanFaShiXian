function DelLPNInCLRU()
% 该函数删除CLRU中的LRU位置的数据
 global CLRU;
 if CLRU.size<=0
        return;
 end
 Index=FindMinAge(CLRU.list);
 Victim=CLRU.list(Index);
 ResetNandPage(Victim);
%  注意这里的Victim不是Index
 CLRU.list(Index)=0;
%  CLRU.size=CLRU.size-1;
 CLRU.size=sum(CLRU.list>0);
% 错误检测,判断CLRU.list实际的非零项是否和CLRU.size一直
    NL=sum(CLRU.list>0);
   if CLRU.size~=NL
       fprintf('error happend in DelLPNInCLRU\n');
       error('the CLRU nozero Num is %d\t the CLRU size is %d\n',NL,CLRU.size);
   end
end
function DelLPNInDLRU()
% 该函数删除CLRU中的LRU位置的数据
global DLRU;
global Stat;
if DLRU.size<=0
    return;
end
Index=FindMinAge(DLRU.list);
Victim=DLRU.list(Index);
ResetNandPage(Victim);
DLRU.list(Index)=0;
%  DLRU.size=DLRU.size-1;
 DLRU.size=sum(DLRU.list>0);
% 错误检测,判断CLRU.list实际的非零项是否和CLRU.size一直
    NL=sum(DLRU.list>0);
   if DLRU.size~=NL
       fprintf('error happend in DelLPNInDLRU\n');
       error('the DLRU nozero Num is %d\t the DLRU size is %d\n',NL,DLRU.size);
   end
% ------------------------------------------------------
Stat.write_back_count=Stat.write_back_count+1;
Stat.physical_write_count=Stat.physical_write_count+1;




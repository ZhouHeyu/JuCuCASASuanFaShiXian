function AddNewToBuffer(ReqLPN)
% 该函数主要负责将缺失的数据页加载到Buffer中，同时完成读写miss的统计及物理读请求统计
%   和外部数据交换的全局变量
    global Stat;
    global CLRU;
    global DLRU;
    global NandPageArr;
    global CacheMaxAgeIndex;
% 和NandPage.Cache_Stat有关的宏定义（在不在缓冲区,在哪个队列）
    global Cache_InCLRU;
    global Cache_InDLRU;
    if ReqLPN.type~=0
%       统计信息的更新
        Stat.read_miss_count=Stat.read_miss_count+1;
        Stat.physical_read_count=Stat.physical_read_count+1;
%       数据加载操作
        free_pos=FindFreePos(CLRU.list);
        CLRU.list(free_pos)=ReqLPN.start;
%       针对对一次寻找最大的CacheMaxAgeIndex=0，需要处理
        if CacheMaxAgeIndex==0
            NandPageArr(ReqLPN.start).Cache_Age=1;
        else
             NandPageArr(ReqLPN.start).Cache_Age=NandPageArr(CacheMaxAgeIndex).Cache_Age+1;
        end
        CacheMaxAgeIndex=ReqLPN.start;
        NandPageArr(ReqLPN.start).Cache_Stat=Cache_InCLRU;
        NandPageArr(ReqLPN.start).Cache_Update=0;
%         CLRU.size=CLRU.size+1;
        CLRU.size=sum(CLRU.list>0);
%------------------ 错误检测,判断CLRU.list实际的非零项是否和CLRU.size一致----------------
        NL=sum(CLRU.list>0);
           if CLRU.size~=NL
               fprintf('error happend in AddNewToBuffer\n');
               error('the CLRU nozero Num is %d\t the CLRU size is %d\n',NL,CLRU.size);
           end
%--------------------------------------------------------------------------------------
    else
%       统计信息的更新 
        Stat.write_miss_count=Stat.write_miss_count+1;
        Stat.physical_read_count=Stat.physical_read_count+1;
%       数据加载操作       
        free_pos=FindFreePos(DLRU.list);
        DLRU.list(free_pos)=ReqLPN.start;
%       针对对一次寻找最大的CacheMaxAgeIndex=0，需要处理
        if CacheMaxAgeIndex==0
            NandPageArr(ReqLPN.start).Cache_Age=1;
        else
             NandPageArr(ReqLPN.start).Cache_Age=NandPageArr(CacheMaxAgeIndex).Cache_Age+1;
        end
        CacheMaxAgeIndex=ReqLPN.start;
        NandPageArr(ReqLPN.start).Cache_Stat=Cache_InDLRU;
        NandPageArr(ReqLPN.start).Cache_Update=1;
%         DLRU.size=DLRU.size+1;
        DLRU.size=sum(DLRU.list>0);
%------------------ 错误检测,判断CLRU.list实际的非零项是否和DLRU.size一致----------------
        NL=sum(DLRU.list>0);
           if DLRU.size~=NL
               fprintf('error happend in AddNewToBuffer\n');
               error('the DLRU nozero Num is %d\t the DLRU size is %d\n',NL,DLRU.size);
           end
%--------------------------------------------------------------------------------------
    end
end
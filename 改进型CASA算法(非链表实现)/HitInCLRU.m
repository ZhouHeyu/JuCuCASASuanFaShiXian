function HitInCLRU(ReqLPN)
% 该函数处理命中CLRU队列的请求
% 和外界数据交换的全局变量
    global NandPageArr;
    global CacheMaxAgeIndex;
    global CLRU;
    global DLRU;
    global Stat;
    global CDHit;
    % 和NandPage.Cache_Stat有关的宏定义（在不在缓冲区,在哪个队列）
    global Cache_Invalid;
    global Cache_InCLRU;
    global Cache_InDLRU;
    NandPageArr(ReqLPN.start).Cache_Age=NandPageArr(CacheMaxAgeIndex).Cache_Age+1;
%   更新当前的CacheMax为当前的LPN
    CacheMaxAgeIndex=ReqLPN.start;
%   区分命中的是W/R?
    if ReqLPN.type==0
%       写命中移动到DLRU，删除CLRU
        Index=FindValueInArr(CLRU.list,ReqLPN.start);
        CLRU.list(Index)=0;
%       将其存入DLRU
        free_pos=FindFreePos(DLRU.list);
        DLRU.list(free_pos)=ReqLPN.start;
%       更新相应的标志位
        NandPageArr(ReqLPN.start).Cache_Stat=Cache_InDLRU;
        NandPageArr(ReqLPN.start).Cache_Update=1;
        Stat.write_hit_count=Stat.write_hit_count+1;
        CDHit.CWH=CDHit.CWH+1;
    else
        Stat.read_hit_count=Stat.read_hit_count+1;
        CDHit.CRH=CDHit.CRH+1;
%       不需要移动DLRU
    end    
end
function HitInCLRU(ReqLPN)
% 该函数处理命中CLRU队列的请求
% 和外界数据交换的全局变量
    global NandPageArr;
    global CacheMaxAgeIndex;
    global CLRU;
    global DLRU;
    global Stat;
    global CDHit;
    global BlkTableArr;
    % 和NandPage.Cache_Stat有关的宏定义（在不在缓冲区,在哪个队列）
    global Cache_Invalid;
    global Cache_InCLRU;
    global Cache_InDLRU;
    global PAGE_NUM_PER_BLK;
    NandPageArr(ReqLPN.start).Cache_Age=NandPageArr(CacheMaxAgeIndex).Cache_Age+1;
%   更新当前的CacheMax为当前的LPN
    CacheMaxAgeIndex=ReqLPN.start;
%   区分命中的是W/R?
    if ReqLPN.type==0
%       写命中移动到DLRU，删除CLRU
        Index=FindValueInArr(CLRU.list,ReqLPN.start);
        CLRU.list(Index)=0;
        CLRU.size=sum(CLRU.list>0);
%       将其存入DLRU
        free_pos=FindFreePos(DLRU.list);
        DLRU.list(free_pos)=ReqLPN.start;
        DLRU.size=sum(DLRU.list>0);
%       更新相应的标志位
        NandPageArr(ReqLPN.start).Cache_Stat=Cache_InDLRU;
        NandPageArr(ReqLPN.start).Cache_Update=1;
        Stat.write_hit_count=Stat.write_hit_count+1;
        CDHit.CWH=CDHit.CWH+1;
% --------------------聚簇算法嵌入代码-------------------------------------------------
%       数据从CLRU移动到DLRU则需要更新BlkTableArr的数据
%       计算更新对应的块索引标记
        tempBlkNum=fix(ReqLPN.start/PAGE_NUM_PER_BLK)+1;
        BlkTableArr(tempBlkNum).CleanNum=BlkTableArr(tempBlkNum).CleanNum-1;
        BlkTableArr(tempBlkNum).DirtyNum=BlkTableArr(tempBlkNum).DirtyNum+1;
%       在块对应的Clist删除相应的CLRU位置索引Index
        BlkTableArr(tempBlkNum).Clist=DelVauleInArr(BlkTableArr(tempBlkNum).Clist,Index);
%       在块对应的Dlist中加入相应的DLRU位置索引free_pos
        BlkTableArr(tempBlkNum).Dlist=[free_pos,BlkTableArr(tempBlkNum).Dlist];
%-----------------------------做一个错误检测----------------------------------------------
        Clist_length=length(BlkTableArr(tempBlkNum).Clist);
        Dlist_length=length(BlkTableArr(tempBlkNum).Dlist);
        if(BlkTableArr(tempBlkNum).TotalSize~=Clist_length+Dlist_length)
            fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum, Dlist_length);
            fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
            fprintf('BlkTable(%d) -TotalSize is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).TotalSize);
            error('the add Index into BlkTableArr exist error!');
        end
        if (BlkTableArr(tempBlkNum).CleanNum~=Clist_length||BlkTableArr(tempBlkNum).DirtyNum~=Dlist_length)
            fprintf('BlkTable(%d) -CleanNum is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).CleanNum);
            fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
            fprintf('BlkTable(%d) -DirtyNum is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).DirtyNum);
            fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum,Dlist_length)
            error('the add Index into BlkTableArr exist error!');
        end
%-------------------------------错误检测结束------------------------------------------------
    else
        Stat.read_hit_count=Stat.read_hit_count+1;
        CDHit.CRH=CDHit.CRH+1;
%       不需要移动DLRU
    end    
end
function [Vlist,KeepList]=FindVictimCluster(Dlist,BoundValue)
% 该函数对输入的参数（一般是对应块中所属的脏队列页索引）
% 输出的参数Vlist就是要剔除那些冷页(age<BoundValue)在DLRU.list中的索引
% KeepList是要保留的热数据页
    global NandPageArr;
    global DLRU;
    Vlist=[];
    KeepList=[];
    L=length(Dlist);
%    Dlist(index)存放的是该脏页LPN在DLRU中位置索引，并不是LPN号
    for index=1:L
        tempLPN=DLRU.list(Dlist(index));
        if NandPageArr(tempLPN).Cache_Age<=BoundValue
%             如果对应的tempLPN在缓冲区的时间小于Bound,则作为删除的候选项,存的也是DLRU.list的位置索引
              Vlist=[Vlist,Dlist(index)];
        else
%             反之作为保留的对象
              KeepList=[KeepList,Dlist(index)];
        end
    end
%     如果删除的Vlist的大小过于小，或者大于DLRU.list中的最大值，做一个错误检测输出
    if (length(Vlist)==0 || length(Vlist)>length(Dlist)|| length(Vlist)>length(DLRU.list))
           fprintf('the error happend in FindVictimCluster\n');
           fprintf('the Vlist-size is %d\n',length(Vlist));
           fprintf('the Dlist-size is %d\n',length(Dlist));
           fprintf('the DLRU-list-size is %d\n',length(DLRU.list));
    end
end
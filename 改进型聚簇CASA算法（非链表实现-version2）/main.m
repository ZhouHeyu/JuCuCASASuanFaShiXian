% 这里的LRU算法和以往实现的方法(链表法)不一样，这里比较age最大
clc
clear all
% 输入测试文件和输出的结果文件
trace_filename='..\..\test_trace\Financial1';%配置输入的trace文件
output_filename='.\Result\Fin1_cluster64_CASA_RW15_buf4096_threshold0.5_var.mat';%设置输出的指定文件
global ReqSec;
[ReqSec.arrive,ReqSec.device_num,ReqSec.start,ReqSec.size,ReqSec.type]=textread(trace_filename,'%f%d%d%d%d');
% %读取trace文件中的五个参数（请求到达时间，设备号，请求扇区号起始位置，请求扇区大小，请求操作类型）
%%
% 相关的宏定义
% 配置相关数据对齐的操作
global SECT_NUM_PER_PAGE;
SECT_NUM_PER_PAGE=4;
global PAGE_NUM_PER_BLK;
PAGE_NUM_PER_BLK=64;
global SECT_NUM_PER_BLK;
SECT_NUM_PER_BLK=SECT_NUM_PER_PAGE*PAGE_NUM_PER_BLK;
% 和NandPage.Cache_Stat有关的宏定义（在不在缓冲区,在哪个队列）
global Cache_Invalid;
Cache_Invalid=0;
global Cache_InCLRU;
Cache_InCLRU=1;
global Cache_InDLRU;
Cache_InDLRU=2;
% 关于读写代价的设置和写放大系数的调整
global FlashParameter;
FlashParameter.rCost=50;
FlashParameter.wCost=250;
FlashParameter.wAmp =1.35;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 计算得到访问请求最大的扇区号
MaxSecNum=max(ReqSec.start+ReqSec.size);
% 逻辑页号从1开始
MaxPageNum=fix(MaxSecNum/SECT_NUM_PER_PAGE)+1;
% 计算得到的最大块号,逻辑块号从1开始
MaxBlkNum=fix(MaxPageNum/PAGE_NUM_PER_BLK)+1;
% 初始化所有的LPN页数组
global NandPageArr;
NandPageArr=InitNandPage(MaxPageNum);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   加入聚簇的blk_table的数据索引项
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 以空间换时间，将所有的数据块都存储，下标即块号
global BlkTableArr;
BlkTableArr=InitBlkTableArr(MaxBlkNum);

% 关键的全局变量定义
% Stat全局统计信息的结构体
global Stat;
global buf_size;
global CLRU;
global DLRU;
% 设计缓冲区的大小
buf_size=1024*4;
% tau是目标干净页CLRU队列的长度，初始化tau的长度是总缓冲区的一半
tau=buf_size/2;
% 聚簇删除的判断的阈值百分比threshold
global threshold;
threshold=1/2;
% 设置调整tau的周期
ObverseCircle=1*buf_size;
% 初始化CLRU，DLRU，Stat这些结构体
DLRU_Initalize();
CLRU_Initialize();
Stat_Initalize();

%StatLast作为上次统计的情况
StatLast=Stat;
% 设定观察周期
Interval=1000;
%对请求扇区序列进行次数统计,有多少行就是有多少次请求
% all_req_sec_num=min(10^6,length(ReqSec.start));
all_req_sec_num=length(ReqSec.start);
%增加存储每个Interval期间内的Write/ReadHitCount/MissCount的情况
global history;
InitHistoryStat(round(all_req_sec_num/Interval));
%新添加的Tau_adj在新的计算Tau算法时需要用到
Tau_adj = buf_size;
% 定义缓冲区命中的类型
% CRH-CLRU读命中；CWH-CLRU写命中；DRH-DLRU读命中；DWH-DLRU写命中;UH表示未命中
% 直接将以下这些参数写成一个全局变量的结构体
% 我自己的Tau值计算需要用到的统计变量
global CDHit;
CDHit.CWH=0;
CDHit.CRH=0;
CDHit.DWH=0;
CDHit.DRH=0;
% 老师的Tau值计算需要用的统计变量
CLRU_Hit = 0;
DLRU_Hit = 0;

T_Count = 0;
Interval_index=0;
SysTime=0;
% CacheMaxAgeIndex是指对应的LPN号
global CacheMaxAgeIndex;
CacheMaxAgeIndex=0;
% 记录按簇大小删除的次数
global DelClusterCount;
DelClusterCount=zeros(1,PAGE_NUM_PER_BLK);



tic
% 开始请求处理
for req_index=1:all_req_sec_num
%   统计和输出观察周期内的统计数据
    if mod(req_index,Interval)==0
        Interval_index=Interval_index+1;
        fprintf('req=%d, CLRU_length=%d, DLRU_length=%d, tau=%d\n',req_index,CLRU.size,DLRU.size,round(tau));        
        CurrStat.ReadHitCount=Stat.read_hit_count-StatLast.read_hit_count;
        CurrStat.ReadMissCount=Stat.read_miss_count-StatLast.read_miss_count;
        CurrStat.WriteHitCount=Stat.write_hit_count-StatLast.write_hit_count;
        CurrStat.WriteMissCount=Stat.write_miss_count-StatLast.write_miss_count;
        %Interval期间内发生了多少次的页请求
        CurrStat.TotalCount    =Stat.all_page_req_num-StatLast.all_page_req_num;
        CurrStat.AveDelay=(double(CurrStat.ReadMissCount*FlashParameter.rCost+CurrStat.WriteMissCount*FlashParameter.wCost*FlashParameter.wAmp)/CurrStat.TotalCount);
        DisplayCurrStat(CurrStat);
        UpdateHistoryStat(Interval_index,CurrStat,tau,CLRU.size,DLRU.size);
        StatLast=Stat;
    end
%   统计和输出观察操作结束
    %request_index表示当前请求在总请求序列中的次序
    %对扇区请求页对齐
    [ReqLPN]=PageAlignment(req_index);
    Stat.all_page_req_num=Stat.all_page_req_num+ReqLPN.size;%统计所有的页请求次数
    CurSize = ReqSec.size(req_index);
%   开始处理一个req请求
    while(ReqLPN.size>0)
         SysTime=SysTime+1;
%       （所有的请求LPN都偏移加1），开始新的页请求处理
        ReqLPN.start=ReqLPN.start+1;
%========================判断请求是否在buf中================================
        if(NandPageArr(ReqLPN.start).Cache_Stat~=Cache_Invalid)
            Stat.hit_count=Stat.hit_count+1;
%       ----------------命中缓冲区操作完成----------------------------
            if NandPageArr(ReqLPN.start).Cache_Stat==Cache_InCLRU
%       -----------------------命中DLRU的操作------------------------
                CLRU_Hit=CLRU_Hit+1;
                HitInCLRU(ReqLPN);
            else
%       -----------------------命中DLRU的操作------------------------
                DLRU_Hit=DLRU_Hit+1;
                HitInDLRU(ReqLPN);
%       -----------------------命中DLRU的操作完成----------------------
            end%-命中缓冲区操作完成
%               更新tau,传统的CASA算法在此嵌入代码
%       -----------------命中缓冲区操作完成--------------------------------        
        else
%       -----------------没有命中缓冲区------------------------
            Stat.miss_count=Stat.miss_count+1;
%       ------------如果缓冲区满，则选择剔除-----------------
            if CLRU.size+DLRU.size>=buf_size
%                 依据Tau值选择性剔除
% ------------------------------DEBUG-------------------------------------------
%              加入代码调试判断CLRU和DLRU的实际大小时候符合预期
           if (sum(CLRU.list>0)~=CLRU.size||sum(DLRU.list>0)~=DLRU.size)
               fprintf('the CLRU.size is %d\n',CLRU.size);
               fprintf('the DLRU.list noezeors entry num is %d\n',sum(DLRU.list>0));
               fprintf('the CLRU.list nozeros entry num is %d\n',sum(CLRU.list>0));
               fprintf('the DLRU.size is %d\n',DLRU.size);
               error('find error in delLPN in Main\n');
           end
%   --------------------------DEBUG-----------------------------------------
                 if CLRU.size>=tau
%                      tic
                       DelLPNInCLRU();
%                      toc
                 else
%       --------选择DLRU作为剔除对象，聚簇剔除操作---------
                        DelLPNInDLRU();
%  ------------------------------DEBUG-------------------------------------------
%              加入代码调试判断CLRU和DLRU的实际大小时候符合预期
           if (sum(CLRU.list>0)~=CLRU.size||sum(DLRU.list>0)~=DLRU.size)
               fprintf('the CLRU.size is %d\n',CLRU.size);
               fprintf('the DLRU.list noezeors entry num is %d\n',sum(DLRU.list>0));
               fprintf('the CLRU.list nozeros entry num is %d\n',sum(CLRU.list>0));
               fprintf('the DLRU.size is %d\n',DLRU.size);
               error('find error in DelLPNDLRU operation');
           end
%   --------------------------DEBUG-----------------------------------------
                 end
            end
%       -----------如果缓冲区满，剔除完成-------------------
                AddNewToBuffer(ReqLPN);
%       ----------没有命中缓冲区---------------------------------
        end
%   缓冲区的数据命中-未命中操作结束      
%%%%%%%%%%%%%%%%改进后的Tau值计算的代码嵌入%%%%%%%%%%%%%%%%%%%%%
          T_Count=T_Count+1;
          if T_Count==ObverseCircle
            [tau]=UpdateTau3(tau);
%------------------如果采用老师的tau更新需要更新的值-----
            CLRU_Hit = 0;
            DLRU_Hit = 0;
%---------------我的tau计算需要更新的值------------------
            CDHit.CRH=0;
            CDHit.CWH=0;
            CDHit.DRH=0;
            CDHit.DWH=0;
            T_Count  = 0;
          end
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 
 
%        开始下一个页请求的处理
        ReqLPN.size=ReqLPN.size-1;%请求页完成一次，大小减一
    end%---of-while
end%----end-for

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% 输出数据
PrintResult(Stat);
% 保存有效的结果日后分析
clear ReqSec;
clear CLRU DLRU;
save(output_filename);
toc

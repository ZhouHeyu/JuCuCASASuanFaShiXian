function DelLPNInDLRU()
% 全局的宏定义
global Cache_InCLRU;
global PAGE_NUM_PER_BLK;
% 该函数删除CLRU中的LRU位置的数据
global DLRU;
global CLRU;
global Stat;
global BlkTableArr;
global CacheMaxAgeIndex;
global threshold;
global NandPageArr;
global DelClusterCount;

if DLRU.size<=0
    return;
end
% %单一页的删除操作
% Index=FindMinAge(DLRU.list);
% Victim=DLRU.list(Index);
% ResetNandPage(Victim);
% DLRU.list(Index)=0;
% DLRU.size=sum(DLRU.list>0);
% Stat.write_back_count=Stat.write_back_count+1;
% Stat.physical_write_count=Stat.physical_write_count+1;
% ------------------------------------------------------

%老师的聚簇算法的想法是找到一个脏页要剔除的时候，查看其相应的数据页的冷热，冷的一起回写，
% -----------------聚簇算法代码嵌入---------------------
% Index=FindMinAge(DLRU.list);一般的找到最小值函数不适用，功能改进，希望同时得到其中的中位值是多少
% 或者排前X%的分界值是多少的函数
    [MinIndex,BoundValue]=FindMinAndBound(DLRU.list,threshold);
%   定位到要剔除的数据项
    Victim=DLRU.list(MinIndex);
%   查看所属的Blk(tempBlkNum)的一样的脏页哪些小于BoundValue一起回写剔除
%   计算更新对应的块索引标记
    tempBlkNum=fix(Victim/PAGE_NUM_PER_BLK)+1;
%   根据该块号，查看相关的哪些脏页小于BoundValue
%   选择删除,VictimCluster是DLRU.list中要删除的数据页的索引，不是LPN号
    [VictimCluster,KeepList]=FindVictimCluster(BlkTableArr(tempBlkNum).Dlist,BoundValue);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% %                          这里做两个对比，一个是只删除冷页，
% %   -----------------------------Version1----------------------------------
%     Stat.write_back_count=Stat.write_back_count+length( VictimCluster);
%     Stat.physical_write_count=Stat.physical_write_count+length( VictimCluster);
% %   统计相应簇回写的大小
%     DelClusterCount(length( VictimCluster))=DelClusterCount(length( VictimCluster))+1;
% %   依次遍历定位到DLRU.list中的数据页，删除，同时置位NandPageArr中的值
%     for i=1:length(VictimCluster)
% %       tempVictim只是DLRU.list的位置索引不是,LPN
%         tempVictim=VictimCluster(i);
%         tempLPN=DLRU.list(tempVictim);
%         ResetNandPage(tempLPN);
%         DLRU.list(tempVictim)=0;
% %       在块对应的BlkTableArr(tempBlkNum).Dlist删除相应的CLRU位置索引Index(tempVictim),这段代码的执行效率比较低
%         BlkTableArr(tempBlkNum).Dlist=DelVauleInArr(BlkTableArr(tempBlkNum).Dlist,tempVictim);
%     end
%     DLRU.size=sum(DLRU.list>0);
% %    同时需要注意更新 BlkTableArr(tempBlkNum)的相关索引
%     BlkTableArr(tempBlkNum).TotalSize=BlkTableArr(tempBlkNum).TotalSize-length(VictimCluster);
%     BlkTableArr(tempBlkNum).DirtyNum=BlkTableArr(tempBlkNum).DirtyNum-length(VictimCluster);
% %%-----------------------------做一个错误检测----------------------------------------------
%         Clist_length=length(BlkTableArr(tempBlkNum).Clist);
%         Dlist_length=length(BlkTableArr(tempBlkNum).Dlist);
%         if(BlkTableArr(tempBlkNum).TotalSize~=Clist_length+Dlist_length)
%             fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum, Dlist_length);
%             fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
%             fprintf('BlkTable(%d) -TotalSize is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).TotalSize);
%             error('the Del Index into BlkTableArr exist error!');
%         end
%         if (BlkTableArr(tempBlkNum).CleanNum~=Clist_length||BlkTableArr(tempBlkNum).DirtyNum~=Dlist_length)
%             fprintf('BlkTable(%d) -CleanNum is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).CleanNum);
%             fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
%             fprintf('BlkTable(%d) -DirtyNum is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).DirtyNum);
%             fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum,Dlist_length)
%             error('the Del Index into BlkTableArr exist error!');
%         end
% %-------------------------------错误检测结束------------------------------------------------
%     
% %   
% %    --------------------------Version1结束-----------------------------------




%                       另一个同时删除回写热数据页，变成干净页移入到CLRU.list中去(运行时注意注释)
%   -------------------------------Version2------------------------------------------

%   全部的脏页一起回写，但是保留一部分的热数据页变为干净页加入到CLRU.list中
   Stat.write_back_count=Stat.write_back_count+length(BlkTableArr(tempBlkNum).Dlist);
   Stat.physical_write_count=Stat.physical_write_count+length(BlkTableArr(tempBlkNum).Dlist);
%    统计相应的簇回写次数
    DelClusterCount(length(BlkTableArr(tempBlkNum).Dlist))=DelClusterCount(length(BlkTableArr(tempBlkNum).Dlist))+1;
%    现将保留的热数据页移动到CLRU.list,同时注意更新对应的BlkTableArr索引
    for i=1:length(KeepList)
        tempKeep=KeepList(i);
        tempLPN=DLRU.list(tempKeep);
%       对应的NandPage标志位修改Cache_Stat和Cache_Update
        NandPageArr(tempLPN).Cache_Stat=Cache_InCLRU;
        NandPageArr(tempLPN).Cache_Update=0;
%       将其加入到CLRU.list中去
        free_pos=FindFreePos(CLRU.list);
        CLRU.list(free_pos)=tempLPN;
%       更新Clist的索引free_pos
        BlkTableArr(tempBlkNum).Clist=[BlkTableArr(tempBlkNum).Clist,free_pos];
        BlkTableArr(tempBlkNum).CleanNum=BlkTableArr(tempBlkNum).CleanNum+1;
%       删除DLRU.list中的数据和Dlist中的索引tempKeep
        DLRU.list(tempKeep)=0;
        BlkTableArr(tempBlkNum).Dlist=DelVauleInArr(BlkTableArr(tempBlkNum).Dlist,tempKeep);
        BlkTableArr(tempBlkNum).DirtyNum= BlkTableArr(tempBlkNum).DirtyNum-1;
    end
%    注意DLRU.size和CLRU.size的更新
        DLRU.size=sum(DLRU.list>0);
        CLRU.size=sum(CLRU.list>0);    
%   依次遍历定位到DLRU.list中的数据页，删除，同时置位NandPageArr中的值
    for i=1:length(VictimCluster)
%       tempVictim只是DLRU.list的位置索引不是,LPN
        tempVictim=VictimCluster(i);
        tempLPN=DLRU.list(tempVictim);
        ResetNandPage(tempLPN);
        DLRU.list(tempVictim)=0;
%       在块对应的BlkTableArr(tempBlkNum).Dlist删除相应的CLRU位置索引Index(tempVictim),这段代码的执行效率比较低
        BlkTableArr(tempBlkNum).Dlist=DelVauleInArr(BlkTableArr(tempBlkNum).Dlist,tempVictim);
       BlkTableArr(tempBlkNum).DirtyNum= BlkTableArr(tempBlkNum).DirtyNum-1;
    end
        DLRU.size=sum(DLRU.list>0);
%    同时需要注意更新 BlkTableArr(tempBlkNum)的相关索引
        BlkTableArr(tempBlkNum).TotalSize=BlkTableArr(tempBlkNum).TotalSize-length(VictimCluster);
%%-----------------------------做一个错误检测----------------------------------------------
        Clist_length=length(BlkTableArr(tempBlkNum).Clist);
        Dlist_length=length(BlkTableArr(tempBlkNum).Dlist);
        if(BlkTableArr(tempBlkNum).TotalSize~=Clist_length+Dlist_length)
            fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum, Dlist_length);
            fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
            fprintf('BlkTable(%d) -TotalSize is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).TotalSize);
            error('the Del Index into BlkTableArr exist error!');
        end
        if (BlkTableArr(tempBlkNum).CleanNum~=Clist_length||BlkTableArr(tempBlkNum).DirtyNum~=Dlist_length)
            fprintf('BlkTable(%d) -CleanNum is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).CleanNum);
            fprintf('BlkTable(%d) -ClistSize is %d\n',tempBlkNum,Clist_length);
            fprintf('BlkTable(%d) -DirtyNum is %d\n',tempBlkNum,BlkTableArr(tempBlkNum).DirtyNum);
            fprintf('BlkTable(%d) -DlistSize is %d\n',tempBlkNum,Dlist_length)
            error('the Del Index into BlkTableArr exist error!');
        end
%-------------------------------错误检测结束------------------------------------------------
%   --------------------------------Version2------------------------------------------
    
%                       该总体函数的bug调试错误
%  ------------------------------DEBUG-------------------------------------------
%              加入代码调试判断CLRU和DLRU的实际大小时候符合预期
           if (sum(CLRU.list>0)~=CLRU.size||sum(DLRU.list>0)~=DLRU.size)
               fprintf('the CLRU.size is %d\n',CLRU.size);
               fprintf('the CLRU.list nozeros entry num is %d\n',sum(CLRU.list>0)); 
               fprintf('the DLRU.size is %d\n',DLRU.size);
               fprintf('the DLRU.list noezeors entry num is %d\n',sum(DLRU.list>0));
               error('find error in DelLPNInDLRU');
           end
%   --------------------------DEBUG-----------------------------------------

end



